package com.cts.currency.command;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cts.currency.exception.CurrencyConversionException;
import com.cts.currency.facade.CurrencyConverterFacade;

public class CurrencyConverterCommandTest {
	private CurrencyConverterFacade converterCommand;

	@Before
	public void init() {
		converterCommand = new CurrencyConverterFacade();
	}

	@After
	public void destroy() {
		converterCommand = null;
	}

	@Test
	public void testConvertAUD2USD() {
		converterCommand.setInput("AUD 100.00 in USD");
		converterCommand.performConversion();
		String result = converterCommand.getOutput().stringyfy();
		Assert.assertEquals(result, "AUD 100.00 = USD 83.71");
	}

	@Test
	public void testConvertAUD2JPY() {
		converterCommand.setInput("AUD 100.00 in JPY");
		converterCommand.performConversion();
		String result = converterCommand.getOutput().stringyfy();
		Assert.assertEquals(result, "AUD 100.00 = JPY 10041");

	}

	@Test
	public void testConvertAUD2AUD() {
		converterCommand.setInput("AUD 100.00 in AUD");
		converterCommand.performConversion();
		String result = converterCommand.getOutput().stringyfy();
		Assert.assertEquals(result, "AUD 100.00 = AUD 100.00");

	}

	@Test
	public void testConvertAUD2DKK() {
		converterCommand.setInput("AUD 100.00 in DKK");
		converterCommand.performConversion();
		String result = converterCommand.getOutput().stringyfy();
		Assert.assertEquals(result, "AUD 100.00 = DKK 505.76");

	}

	@Test
	public void testConvertJPY2USD() {
		converterCommand.setInput("JPY 100 in USD");
		converterCommand.performConversion();
		String result = converterCommand.getOutput().stringyfy();
		Assert.assertEquals(result, ("JPY 100 = USD 0.83"));

	}

	
	
	@Test
	public void testConvertKRW2FJD_negative() {
		try {
		converterCommand.setInput("KRW 1000.00 in FJD");
		converterCommand.performConversion();
		converterCommand.getOutput();
		}catch(CurrencyConversionException ex) {
			Assert.assertEquals(ex.getMessage(), "Unable to find rate for KRW/FJD.");

		}
	}

	@Test
	public void testConvertJPY2USD_negative() {
		try {
		converterCommand.setInput("JPY 100 in ");
		converterCommand.performConversion();
		converterCommand.getOutput();
		}catch(CurrencyConversionException ex){
			Assert.assertEquals(ex.getMessage(), "Insufficient parameters.");
		}
	}

	
}
