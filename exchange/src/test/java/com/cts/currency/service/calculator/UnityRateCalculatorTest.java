package com.cts.currency.service.calculator;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cts.currency.model.Exchange;


public class UnityRateCalculatorTest {

	private UnityRateCalculatorCommand calc ;


	@Before
	public void init() {
		 calc = new UnityRateCalculatorCommand();
	}

	@Test
	public void calculateRate(){
		Exchange exc = new Exchange("AUD", BigDecimal.ONE, "USD",BigDecimal.ONE);
		Assert.assertEquals(1,calc.execute(exc, 1), 0.0f);
	}
}
