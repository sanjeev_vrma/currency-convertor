package com.cts.currency.service.calculator;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cts.currency.model.Exchange;
import com.cts.currency.service.IRateCalculator;

public class IndirectRateCalculatorTest {
private IRateCalculator calc;
	
	@Before
	public void init(){
		calc = new IndirectRateCalculatorCommand();
	}
	
	@Test
	public void calculateRateTest() {
		Exchange exc = new Exchange("AUD", BigDecimal.ONE, "DKK",BigDecimal.ONE);
		Assert.assertEquals(5.0576067f,calc.execute(exc, 1),0.0000f);
	}
	
	
	@Test
	public void calculateRateTest2() {
		Exchange exc = new Exchange("AUD", BigDecimal.ONE, "JPY",BigDecimal.ONE);
		Assert.assertEquals(100.41015f,calc.execute(exc, 1),0.0000f);
	}
	
}
