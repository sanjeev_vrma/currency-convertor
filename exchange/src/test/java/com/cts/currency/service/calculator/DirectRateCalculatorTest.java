package com.cts.currency.service.calculator;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cts.currency.exception.CurrencyConversionException;
import com.cts.currency.model.Exchange;


public class DirectRateCalculatorTest {

	private DirectRateCalculatorCommand calc;
	
	@Before
	public void init(){
		calc = new DirectRateCalculatorCommand();
	}
	
	@Test
	public void calculateRate() {
		Exchange exc = new Exchange("AUD", BigDecimal.ONE, "USD",BigDecimal.ONE);
		Assert.assertEquals(0.8371f,calc.execute(exc, 1),0.0000f);
	}
	
	@Test(expected =CurrencyConversionException.class)
	public void calculateRate_negative() {
		Exchange exc = new Exchange("JPY", BigDecimal.ONE, "AUD",BigDecimal.ONE);
		calc.execute(exc, 1);
	}
	
}
