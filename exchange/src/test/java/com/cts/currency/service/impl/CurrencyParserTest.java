package com.cts.currency.service.impl;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cts.currency.exception.CurrencyConversionException;
import com.cts.currency.model.Exchange;

public class CurrencyParserTest {
	
	private InputCommandParser parser;

	@Before
	public void init(){
		parser = new InputCommandParser();
	}
	@Test
	public void parseTest() {
		Exchange exchange = parser.parse("AUD 100.00 in DKK");
		Exchange expected  = new Exchange("AUD", new BigDecimal("100.00"), "DKK", null);
		Assert.assertEquals(expected, exchange);
	}
	
	@Test(expected=CurrencyConversionException.class)
	public void parseTest_negative() {
		parser.parse("XYZ 100.00 in DKK");
	}
	
	@Test(expected=CurrencyConversionException.class)
	public void parseTest_negative2() {
		parser.parse("AUD 100.00 in");
	}
}
