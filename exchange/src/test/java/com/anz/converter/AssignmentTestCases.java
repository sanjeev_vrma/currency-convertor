package com.anz.converter;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.cts.currency.command.CurrencyConverterCommandTest;
import com.cts.currency.service.calculator.DirectRateCalculatorTest;
import com.cts.currency.service.calculator.IndirectRateCalculatorTest;
import com.cts.currency.service.calculator.UnityRateCalculatorTest;
import com.cts.currency.service.impl.CurrencyParserTest;

@RunWith(Suite.class)
@SuiteClasses({ CurrencyConverterCommandTest.class,DirectRateCalculatorTest.class,IndirectRateCalculatorTest.class,UnityRateCalculatorTest.class,CurrencyParserTest.class })
public class AssignmentTestCases {

}
