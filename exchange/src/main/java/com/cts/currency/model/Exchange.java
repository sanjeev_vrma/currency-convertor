package com.cts.currency.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.cts.currency.util.CurrencyConstants;

public class Exchange {
	private Currency source;
	private Currency target;
	private float rate;

	public Exchange() {
	}

	public Exchange(String src, String des) {
		this.source = new Currency(CurrencyType.valueOf(src), new BigDecimal(0));
		this.target = new Currency(CurrencyType.valueOf(des), new BigDecimal(0));
	}

	public Exchange(String src, BigDecimal srcVal, String des, BigDecimal destVal) {
		this.source = new Currency(CurrencyType.valueOf(src), srcVal);
		this.target = new Currency(CurrencyType.valueOf(des), destVal);
	}

	public Currency getSource() {
		return source;
	}

	public void setSource(Currency source) {
		this.source = source;
	}

	public Currency getTarget() {
		return target;
	}

	public void setTarget(Currency target) {
		this.target = target;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "Exchange [source=" + source + ", target=" + target + ", rate=" + rate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(rate);
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exchange other = (Exchange) obj;
		if (Float.floatToIntBits(rate) != Float.floatToIntBits(other.rate))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}

	public String stringyfy() {
		StringBuilder output = new StringBuilder();
		DecimalFormat outputSourceDecimalformatter = currencyFormatter(this.getSource().getType().getDecmalValue());
		DecimalFormat outputTargetDecimalformatter = currencyFormatter(this.getTarget().getType().getDecmalValue());
		output.append(this.getSource().getType().name()
				+ " "
				+ outputSourceDecimalformatter.format(this.getSource().getValue().doubleValue())
				+ " = "
				+ this.getTarget().getType().name()
				+ " "
				+ outputTargetDecimalformatter.format(this.getTarget().getValue()
						.doubleValue()));
		return output.toString().trim();
	}

	private DecimalFormat currencyFormatter(int decimalPoint) {
		DecimalFormat outputDecimalformatter = new DecimalFormat(
				CurrencyConstants.DECIMAL_FORMAT);
		outputDecimalformatter.setMaximumFractionDigits(decimalPoint);
		return outputDecimalformatter;
	}
	
}
