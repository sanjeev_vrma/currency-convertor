package com.cts.currency.model;

public enum CurrencyType {

	AUD(2), CAD(2), CNY(2), CZK(2), DKK(2), EUR(2), GBP(2), JPY(0), NOK(2), NZD(2), USD(2);
	private final int decmalValue;

	CurrencyType(int val) {
		this.decmalValue = val;
	}

	public int getDecmalValue() {
		return decmalValue;
	}

}
