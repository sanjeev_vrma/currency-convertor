package com.cts.currency.service.calculator;

import com.cts.currency.model.Exchange;
import com.cts.currency.service.IRateCalculator;
import com.cts.currency.service.calculator.factory.CalculatorFactory;
import com.cts.currency.util.CurrencyStaticCache;

public class IndirectRateCalculatorCommand extends AbstractRateCalculatorCommand {
	private static final CurrencyStaticCache INSTANCE = CurrencyStaticCache.getInstance();

	
	public float execute(Exchange exchange, float initrate) {
		String source = exchange.getSource().getType().name();
		String destination = exchange.getTarget().getType().name();
		return calculateRate(source, destination, initrate);
	}

	private float calculateRate(String source, String destination, float initRate) {

		float rate = initRate;
		if (INSTANCE.currencyRateDefined(source + destination)) {
			rate = getDirectRate(source, destination, initRate);
		} else if (INSTANCE.currencyRateDefined(destination + source)) {
			rate = getInverseRate(destination, source, rate);
		} else {
			String mapOption = INSTANCE.getCurrencyBasetTerm(source + destination);
			if (INSTANCE.currencyRateDefined(source + mapOption)) {
				rate = getDirectRate(source, mapOption, rate);
			} else if (INSTANCE.currencyRateDefined(mapOption + source)) {
				rate = getInverseRate(mapOption, source, rate);
			}
			IRateCalculator calc = CalculatorFactory.getRateCalculator(mapOption, destination);
			Exchange newExchange = new Exchange(mapOption, destination);
			rate = calc.execute(newExchange, rate);
//			rate = calculateRate(mapOption, destination, rate);
		}

		return rate;

	}

}
