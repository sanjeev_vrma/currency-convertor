package com.cts.currency.service;

import com.cts.currency.model.Exchange;

public interface ExchangeService {
	
	void convertAndUpdate(Exchange exchange);

}
