package com.cts.currency.service.calculator;

import com.cts.currency.exception.CurrencyConversionException;
import com.cts.currency.service.IRateCalculator;
import com.cts.currency.util.CurrencyStaticCache;

public abstract class AbstractRateCalculatorCommand implements IRateCalculator {
	private static final CurrencyStaticCache INSTANCE = CurrencyStaticCache.getInstance();

	protected float getInverseRate(String source, String destination, float rate) {
		return rate / getCurrencyRate(source, destination);
	}

	protected float getDirectRate(String source, String destination, float rate) {
		float cr = getCurrencyRate(source, destination);
		return rate * cr;
	}

	protected float getCurrencyRate(String s, String d) {
		String conversionRate = INSTANCE.getCurrencyRate(s + d);
		if(conversionRate == null || conversionRate.isEmpty()) {
			conversionRate= INSTANCE.getCurrencyRate( d+s);
		}
		if(conversionRate == null || conversionRate.isEmpty())
			throw new CurrencyConversionException("Conversion rate not found.");
		return Float.parseFloat(conversionRate);
	}

}
