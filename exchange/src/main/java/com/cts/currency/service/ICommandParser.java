package com.cts.currency.service;

import com.cts.currency.model.Exchange;

public interface ICommandParser {

	Exchange parse(String input);
}
