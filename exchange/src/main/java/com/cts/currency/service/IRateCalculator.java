package com.cts.currency.service;

import com.cts.currency.model.Exchange;

public interface IRateCalculator {
	
    float execute(Exchange exchange, float initRate);
}
