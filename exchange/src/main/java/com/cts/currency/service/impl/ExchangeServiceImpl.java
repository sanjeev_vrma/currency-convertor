package com.cts.currency.service.impl;

import java.math.BigDecimal;

import com.cts.currency.model.Exchange;
import com.cts.currency.service.ExchangeService;
import com.cts.currency.service.IRateCalculator;
import com.cts.currency.service.calculator.factory.CalculatorFactory;

public class ExchangeServiceImpl implements ExchangeService {

	public void convertAndUpdate(Exchange exchange) {
		float rate = calculateRate(exchange);
		exchange.setRate(rate);
		exchange.getTarget().setValue(calculateTargetCurrencyValue(exchange));
	}

	private float calculateRate(Exchange exchange) {
		String src = exchange.getSource().getType().name();
		String dest = exchange.getTarget().getType().name();
		IRateCalculator calc = CalculatorFactory.getRateCalculator(src, dest);
		float rate = calc.execute(exchange, 1);
		return rate;
	}

	/**
	 * targetVal = rate * sourceCurrencyValue
	 * 
	 * @param exchange
	 */
	private BigDecimal calculateTargetCurrencyValue(Exchange exchange) {
		return new BigDecimal(
				exchange.getSource().getValue().multiply(new BigDecimal(exchange.getRate())).doubleValue());

	}
}
