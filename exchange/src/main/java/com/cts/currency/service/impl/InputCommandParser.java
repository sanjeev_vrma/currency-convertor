package com.cts.currency.service.impl;

import java.math.BigDecimal;

import com.cts.currency.exception.CurrencyConversionException;
import com.cts.currency.model.Currency;
import com.cts.currency.model.CurrencyType;
import com.cts.currency.model.Exchange;
import com.cts.currency.service.ICommandParser;
import com.cts.currency.util.CurrencyConstants;


/**
 * @author 430845 : CurrencyParserImpl is a input parser
 *         to tokenize the input parameter with specified delimiter
 * 
 */
public class InputCommandParser implements ICommandParser{

	
	@Override
	public Exchange parse(String input){
		 
		String []inputToken= input.split(CurrencyConstants.TOKEN_DELIMITER);
		if (inputToken.length == 4) {
			return constructExchange(inputToken);
		}else {
			throw new CurrencyConversionException("Insufficient parameters.");
		}
	}
	
	/**
	 * Exception case. When Currnecy not supported.
	 * @param inputToken
	 * @return
	 */

	private Exchange constructExchange(String[] inputToken) {
		Exchange exchange =new Exchange();
		Currency sourceCurrency=new Currency();
		
		sourceCurrency.setValue(new BigDecimal(inputToken[1]));
		Currency targetCurrency=new Currency();
		try {
		sourceCurrency.setType(CurrencyType.valueOf(inputToken[0]));
		targetCurrency.setType(CurrencyType.valueOf(inputToken[3]));
		}catch(Exception ex) {
			throw new CurrencyConversionException(String.format("Unable to find rate for %s/%s.", inputToken[0],inputToken[3]) )  ;
		}
		exchange.setSource(sourceCurrency);
		exchange.setTarget(targetCurrency);
		return exchange;
	}

}
