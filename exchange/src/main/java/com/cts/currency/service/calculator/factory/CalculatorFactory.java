package com.cts.currency.service.calculator.factory;

import com.cts.currency.service.IRateCalculator;
import com.cts.currency.service.calculator.DirectRateCalculatorCommand;
import com.cts.currency.service.calculator.IndirectRateCalculatorCommand;
import com.cts.currency.service.calculator.InverseRateCalculatorCommand;
import com.cts.currency.service.calculator.UnityRateCalculatorCommand;
import com.cts.currency.util.CurrencyStaticCache;

public class CalculatorFactory {
	
	private static final CurrencyStaticCache INSTANCE = CurrencyStaticCache.getInstance();

	public static IRateCalculator getRateCalculator(String src, String dest) {
		String term = INSTANCE.getCurrencyBasetTerm(src+dest);
		switch (term) {
		case "1:1":
			return new UnityRateCalculatorCommand();
		case "D":
			return new DirectRateCalculatorCommand();
		case "Inv":
			return new InverseRateCalculatorCommand();
		default:
			return new IndirectRateCalculatorCommand();

	}
		
	}
}
