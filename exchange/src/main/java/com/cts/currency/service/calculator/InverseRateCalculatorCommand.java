package com.cts.currency.service.calculator;

import com.cts.currency.model.Exchange;

public class InverseRateCalculatorCommand extends AbstractRateCalculatorCommand {

	public float execute(Exchange exchange, float initrate) {
		String src = exchange.getSource().getType().name();
		String trgt = exchange.getTarget().getType().name();
		float rate = getInverseRate(src, trgt, initrate);
		exchange.setRate( rate);
		return rate;
	}

}
