package com.cts.currency.service.calculator;

import com.cts.currency.model.Exchange;
import com.cts.currency.service.IRateCalculator;

public class UnityRateCalculatorCommand implements IRateCalculator{

	@Override
	public float execute(Exchange exchange, float initrate) {
		exchange.setRate(1);
		return 1;
	}

	
}
