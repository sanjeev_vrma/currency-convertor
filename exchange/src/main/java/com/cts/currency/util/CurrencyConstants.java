package com.cts.currency.util;

/**
 * @author 430845 : Currency constants refers to the constants used in the
 *         application
 * 
 */
public interface CurrencyConstants {
	/**
	 * Standard delimiter to tokenize the input string
	 */
	String TOKEN_DELIMITER = " ";
	/**
	 * Base Term Mapping property file name to lookup
	 */
	String BASE_TERM_MAPPING_PROPERTIES = "src/main/resources/BaseTermMapping.properties";
	/**
	 * Start of float regex expression to identify decimal formatting
	 */
	String FLOAT_REGEX_WITH_DECIMAL_START = "[0-9]*.[0-9]{0,";
	/**
	 * End of float regex expression to identify decimal formatting
	 */
	String FLOAT_REGEX_WITH_DECIMAL_END = "}";
	/**
	 * Decimal format for generating output in given format
	 */
	String DECIMAL_FORMAT = "0.00";
	/**
	 * Custom currency handler identifier
	 */
	String FX_CURRENCY_HANDLER = "fx";
	/**
	 * Default currency handler identifier
	 */
	String DEFAULT_CURRENCY_HANDLER = "default";
	/**
	 * Currency decimal Mapping property file name to lookup
	 */
	String CURRENCY_DECIMAL_MAPPING_PROPERTIES = "src/main/resources/CurrencyDecimalMapping.properties";
	/**
	 * Currency Conversion Rate Mapping property file name to lookup
	 */
	String CURRENCY_RATE_MAPPING_PROPERTIES = "src/main/resources/CurrencyConversionRateMapping.properties";
	

}
