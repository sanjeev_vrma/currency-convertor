package com.cts.currency.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.cts.currency.exception.CurrencyConversionException;

public class CurrencyStaticCache {

	private static CurrencyStaticCache INSTANCE;
	private CurrencyStaticCache() {
		super();
	}
	
	public static CurrencyStaticCache getInstance() {
		if(null == INSTANCE) {
			synchronized (CurrencyStaticCache.class) {
				if(null == INSTANCE) {
					INSTANCE = new CurrencyStaticCache();
				}				
			}
		}
		return INSTANCE;
	}
	
	private static Properties currencyRateMapperProp = new Properties();
	
	private static Properties currencyBaseTermMap = new Properties();

	static {
		loadCurrencyRateProperties();
		loadCurrencyBaseTermsProperties();
	}

	private static void loadCurrencyRateProperties() {
		try {
			currencyRateMapperProp.load(new FileInputStream(CurrencyConstants.CURRENCY_RATE_MAPPING_PROPERTIES));
		} catch (IOException e) {
			throw new CurrencyConversionException("Error loading static CurrencyConversionRateMapping.properties",e);
		}
	}
	
	private static void loadCurrencyBaseTermsProperties() {
		try {
			currencyBaseTermMap.load(new FileInputStream(CurrencyConstants.BASE_TERM_MAPPING_PROPERTIES));
		} catch (IOException e) {
			throw new CurrencyConversionException("Error loading static Baseterms.properties",e);
		}

	}

	public String getCurrencyRate(String key) {
		return currencyRateMapperProp.getProperty(key);
	}
	
	public boolean currencyRateDefined(String key) {
		return currencyRateMapperProp.containsKey(key);
	}
	
	public String getCurrencyBasetTerm(String key) {
		return currencyBaseTermMap.getProperty(key);
	}
	public boolean currencyBaseTermAvailable(String key) {
		return currencyBaseTermMap.containsKey(key);
	}
}

