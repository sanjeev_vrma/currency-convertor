package com.cts.currency.exception;

/**
 * @author 430845 : CurrencyConversionException is a checked exception which
 *         work as a wrapper exception
 * 
 */
public class CurrencyConversionException extends RuntimeException {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 3751961305476430709L;

	public CurrencyConversionException() {
		super();
	}

	public CurrencyConversionException(String arg0, Throwable arg1,
			boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public CurrencyConversionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CurrencyConversionException(String arg0) {
		super(arg0);
	}

	public CurrencyConversionException(Throwable arg0) {
		super(arg0);
	}

}
