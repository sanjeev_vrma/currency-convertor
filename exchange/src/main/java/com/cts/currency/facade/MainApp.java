package com.cts.currency.facade;

import java.util.Scanner;

public class MainApp {
	public static void main(String[] args) {
		CurrencyConverterFacade command = new CurrencyConverterFacade();
		
		Scanner scan = new Scanner(System.in);
		while(true) {
			try {
				System.out.println("Provide your next input, q to quit:");
				String input = scan.nextLine();
				if(input.equals("q"))
					break;
				command.setInput(input);
				command.performConversion();
				System.out.println(command.getOutput().stringyfy());
			}catch(Exception ex) {
				System.out.println(ex.getMessage());
			}
		}
	}
}
