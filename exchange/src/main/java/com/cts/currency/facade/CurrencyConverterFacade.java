package com.cts.currency.facade;

import com.cts.currency.model.Exchange;
import com.cts.currency.service.ExchangeService;
import com.cts.currency.service.impl.InputCommandParser;
import com.cts.currency.service.impl.ExchangeServiceImpl;

public class CurrencyConverterFacade {
	private Exchange exchange;
	
	private ExchangeService service;
	private InputCommandParser parser;

	private String inputCommand;

	public CurrencyConverterFacade() {
		init();
	}
	/**
	 * it should be injected using DI.
	 */
	private void init() {
		service = new ExchangeServiceImpl();
		parser = new InputCommandParser();
	}

	public void setInput(String input) {
		this.inputCommand = input;
		
		
	}

	public void performConversion() {
		this.exchange = parser.parse(inputCommand);
		service.convertAndUpdate(exchange);
	}

	public Exchange getOutput(){
		return exchange;
	}

}
